import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import AppLauncher 1.0


ApplicationWindow {
    // On the Raspberry, the window should be set as "Fullscreen"
    id: root
    title: qsTr("Hello World")
    width: 800
    height: 480
//    visibility: "FullScreen"

    // The MouseArea is used to control how much time has passed since the user last touched the screen
    MouseArea {
        anchors.fill: parent
        onClicked: {
            timer1.restart()
        }
    }

    ScriptLauncher {
        id: testFunctions
    }

    Rectangle {
        //For the white background
        anchors.fill: parent
        color: "#FFFFFF"
    }

    // These both properties are set for the ToolBarStyle. They both not function properly in the Raspberry
    property Component toolBarStyle1: ToolBarStyle {
        id: toolBarStyle1
        padding {
            left: 8
            right: 8
            top: 3
            bottom: 3
        }
        background: Rectangle {
            color: "#D3D3D3"
            border.color: "#999"
            gradient: Gradient {
                GradientStop { position: 0 ; color: "#fff" }
                GradientStop { position: 1 ; color: "#eee" }
            }
        }
    }
    property Component toolBarStyle2: ToolBarStyle {
        id: toolBarStyle2
        background: Rectangle {
            color: "white"
        }
    }

    // Check if the current page on the stack is "doorClosed", as this is the only page without a ToolBar
    function checkDoorClosedInformation() {
        if (stackView.currentItem.information)
        {
            return stackView.currentItem.information === "doorClosed"
        }
    }

    // Settings for the toolbar
    toolBar: ToolBar {
        id: toolBar
        anchors.fill: parent
        focus: true
//STYLE doesn't work!!
        style: checkDoorClosedInformation() ? toolBarStyle2 : toolBarStyle1


        RowLayout {
            id: layoutToolBar
            anchors.left: parent.left
            visible: checkDoorClosedInformation() ? false:true      // Check the tag of the currentpage on the stack

            ToolButton {
                id: exitButton
                iconName: "Exit"
                iconSource: "qrc:/images/exit1600.png"
                width: 50
                height: 50
                onClicked: {
                    // When clicked, clear all the elements on the stack and push both "initialItem1" and "doorClosed" (in this order)
                    timer1.stop()
                    stackView.clear()
                    stackView.push([initialItem1,"qrc:/doorClosed.qml"])
                }
            }

            ToolButton {
                id: returnButton
                visible: stackView.depth > 1                    // The button only appears when the page isn't "initialItem1"
                antialiasing: true
                transformOrigin: Item.Center
                anchors.right: parent.right
                iconSource: "qrc:/images/return.png"
                iconName: "Return home"
                onClicked:  stackView.pop()
            }
        }
    }

    // Define the stack
    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: initialItem1
    }

    // Define the main window of the program "BDF"
    Item {
        id: initialItem1
        property string information: "root"

        Component.onCompleted: {
            console.log("timer set")
            timer1.start()
        }

        // Settings of the date and time funtion
        Timer {
            id: dateAndTime
            interval: 500
            running: true
            repeat: true
            onTriggered: {
                dateTime2.text = new Date().toLocaleTimeString(Qt.locale("de_DE"), "hh:mm:ss")
                dateTime1.text = new Date().toLocaleDateString(Qt.locale("de_DE"))
            }
        }
        RowLayout {
            anchors.right: parent.right
            anchors.rightMargin: 8

            Text {
                // Date
                id: dateTime1
            }
            Text {
                // Time
                id: dateTime2
                font.bold: true
                style: Text.Normal
                font.pointSize: 9
            }
        }

        // Welcome text and instructions message
        Text {
            id: welcomeText
            x: 250
            text: "Welcome newbie!"
            anchors.verticalCenterOffset: 67
            anchors.topMargin: 80
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: parent.top
            font.pointSize: 17
            font.bold: true
        }
        Text {
            id: introductionText
            x: 140
            width: 483
            height: 364
            wrapMode: Text.WordWrap
            text: "Go ahead and take one of these awesome boxes to start creating cool engineering stuff. Use them wisely! And please, take care of them. Have fun!"
            anchors.topMargin: 130
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: parent.top
            font.italic: true
            font.pointSize: 12
        }

        // Settings of the buttons "LendBox" and "RepairComponent"
        Button {
            id: buttonLendBox
            x: 130
            y: 265
            width: 200
            height: 150
            onClicked: {
                timer1.restart()                        // Restart the timer1, as the user just pressed the touchscreen
                stackView.push("qrc:/lendBox.qml")
                initialItem1.focus = false
            }
            style: ButtonStyle{
                background : Rectangle {
                    width: 500
                    height: 500
                    radius: 4
                    gradient: Gradient {
                        GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                        GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                    }
                }
                label: Text {
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    font.pointSize: 12
                    text: "Lend a box"
                    wrapMode: Text.WordWrap
                }
            }
        }

        Button {
            id: buttonDoorClosed
            x: 410
            y: 265
            width: 200
            height: 150
            onClicked: {
                timer1.stop()
                stackView.push("qrc:/repairComponents.qml")
            }
            style: ButtonStyle{
                background : Rectangle {
                    width: 500
                    height: 500
                    radius: 4
                    gradient: Gradient {
                        GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                        GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                    }
                }
                label: Text {
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    font.pointSize: 12
                    text: "Repair and check components"
                    wrapMode: Text.WordWrap
                }
            }
        }


        // This timer is used for the standby modus. It is set to 1 minute
        Timer {
            id: timer1
            interval: 1*60000    //1 * 60 * 1000 Milliseconds (1 min)
            repeat: false
            onTriggered: {
                if (!checkDoorClosedInformation())
                {
                    // Contrary to the "Exit" button, after the timer limit, the page "doorClosed" will only be added to the stack.
                    // This way the information on the previous page won't be lost
                    stackView.push("qrc:/doorClosed.qml")
                }
                console.log("timer's up")
            }
        }

    }
}
