import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import AppLauncher 1.0

Item {
    id: lendBox
    property string information: "lendBox"          // This tag is used to identify this page on the stack

    property string studentCard
    property string boxNumber

    ScriptLauncher {
        id: testFunctions
    }

    // The MouseArea is used to control how much time has passed since the user last touched the screen
    MouseArea {
        anchors.fill: parent
        onClicked: {
            timer1.restart()
        }
    }

    // Settings of the date and time funtion
    Timer {
        id: dateAndTime
        interval: 500
        running: true
        repeat: true
        onTriggered: {
            dateTime2.text = new Date().toLocaleTimeString(Qt.locale("de_DE"), "hh:mm:ss")
            dateTime1.text = new Date().toLocaleDateString(Qt.locale("de_DE"))
        }
    }
    RowLayout {
        anchors.right: parent.right
        anchors.rightMargin: 8

        Text {
            id: dateTime1
        }
        Text {
            id: dateTime2
            font.bold: true
            style: Text.Normal
            font.pointSize: 9
        }
    }

    // Welcome and instructions message
    Text {
        id: welcomeMessage
        y: 60
        anchors.horizontalCenter: parent.horizontalCenter
        text: "To whom shall we lend the box today?"
        font.pointSize: 20
        anchors.horizontalCenterOffset: 0
    }
    Text {
        id: instructionsMessage
        x: 41
        anchors.top : welcomeMessage.bottom
        font.pointSize: 8
        wrapMode: Text.WordWrap
        text: "Instructions:  "
        anchors.topMargin: 15
    }
    Text {
        width: 477
        height: 48
        anchors.left: instructionsMessage.right
        font.pointSize: 8
        anchors.top : welcomeMessage.bottom
        wrapMode: Text.WordWrap
        text: " Step 1: Scan your StudentCard.
 Step 2: Scan the tag with the Box number.
 Step 3: Click 'accept' to confirm data.
 Step 4: Make sure you wrote down your information correctly.
 Click on the 'clear' button at any time to reset all text fields."
        textFormat: Text.AutoText
        anchors.topMargin: 15
    }

    // Text and TextField for the StudentCard, Boxnumber and "OK/Cancel" buttons
    ColumnLayout {
        x: 38
        y: 223
        width: 563
        height: 218
        RowLayout {
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: -100
            Text {
                text: "StudentCard: "
                font.pointSize: 15
            }
            TextField {
                id: studentCardField
                x: 150
                width: 300
                focus: true
                text: ""
                font.pointSize: 11
                echoMode: 2
                placeholderText: "StudentCard ID"           // Text which appears when the TextField is empty
                onAccepted: {
                    studentCard = studentCardField.text     // When user press "Enter", the "studentcard" info is saved, the next field is enabled and set on focus
                    boxNumberField.enabled = true
                    studentCardField.focus = false
                    boxNumberField.focus = true
                }
                Component.onCompleted: forceActiveFocus()
            }
        }

        RowLayout {
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: -100
            Text {
                text: "Box Number: "
                font.pointSize: 15
            }
            TextField {
                id: boxNumberField
                width: 300
                font.pointSize: 11
                enabled: false
                placeholderText: "Box Number ID"
                onAccepted: {
                    boxNumber = boxNumberField.text         // When user press "Enter", the "boxNumber" info is saved, the next field is enabled and set on focus
                    acceptButton.enabled = true
                    boxNumberField.focus = false
                    acceptButton.focus = true
                }
            }
        }

        // "OK/Cancel" buttons
        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            width: 500
            spacing: 100
            Button {
                id: acceptButton
                text: "Accept"
                onClicked: messageDialog.open()
                enabled: false
            }
            Button {
                text: "Clear"
                onClicked: {
                    resetData()
                }
            }
        }
    }

    // Message dialog to check if the given information is correct, before saving it in the databank
    MessageDialog {
        id: messageDialog
        icon: StandardIcon.Question
        title: "Check the data"
        text: "Are you really sure about that?"
        standardButtons: StandardButton.Yes | StandardButton.Discard
        onYes: {
            console.log("Student " + studentCard + " has the box " + boxNumber)
            resetData()
        }
        onDiscard: {
            resetData()
        }
    }

    // Function used for clearing all fields
    function resetData() {
        studentCardField.text = ""
        boxNumberField.text = ""
        studentCard = ""
        boxNumber = ""
        boxNumberField.enabled = false
        acceptButton.enabled = false
        studentCardField.forceActiveFocus()
    }
}

