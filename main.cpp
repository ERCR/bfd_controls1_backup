#include <QApplication>
#include <QQmlApplicationEngine>
#include "scriptlauncher.h"
#include <QQmlContext>
#include <QtQml>
#include <QStringList>
#include <QtQml/qqml.h>
#include <QGuiApplication>

// Besides the "qmlRegisterType" line and some libraries, everything comes by default.

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<ScriptLauncher>("AppLauncher", 1, 0, "ScriptLauncher");     // Define the class "Scriptlauncher" as a library in qml so it can be used.
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
