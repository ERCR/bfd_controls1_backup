#include "scriptlauncher.h"
#include <QDebug>
#include <QProcess>

//Create an intance of the class and its functions
ScriptLauncher::ScriptLauncher(QObject *parent) :
    QObject(parent),
    m_process(new QProcess(this)),
    m_keyboard(new QProcess(this))
{
}

// The function launchGPIO is started when a user has the rights to open the closet, launching the script which triggers at the same time the GPIO23
void ScriptLauncher::launchGPIO() {
   m_process-> start("/bin/sh", QStringList() << "/home/pi/Documents/BFD/bfd_controls1_backup/script.sh");
   m_process-> waitForFinished(-1);         // Wait for the process to finish (otherwise it won't wait for the gpio to light up)
   qDebug() << "launching App";
}

// The function launchkeyboard is used for the TextField(s) in the "repairComponent" page.
void ScriptLauncher::launchKeyboard(const QString &application) {
    // CHECK matchbox-keyboard window settings in Raspbian (alters the size of maximized windows, making it useless for fullscreen programs)
    m_keyboard->start(application);
    qDebug() << "launching" << application;
}
