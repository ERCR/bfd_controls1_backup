import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import AppLauncher 1.0

Item {
    id: doorClosed
    property string information: "doorClosed"           // This tag is used to identify this page on the stack

    // Define "Scriplauncher" as an object
    ScriptLauncher {
        id: testFunctions
    }

    // Settings of the date and time funtion
    Timer {
        id: dateAndTime

        interval: 500
        running: true
        repeat: true
        onTriggered: {
            dateTime2.text = new Date().toLocaleTimeString(Qt.locale("de_DE"), "hh:mm:ss")
            dateTime1.text = new Date().toLocaleDateString(Qt.locale("de_DE"))
        }
    }
    RowLayout {
        anchors.right: parent.right
        anchors.rightMargin: 8

        Text {
            // Date
            id: dateTime1
        }
        Text {
            // Time
            id: dateTime2
            font.bold: true
            style: Text.Normal
            font.pointSize: 9
        }
    }

    // Welcome and instructions messages
    Text {
        id: textWelcome
        x: 141
        y: 178
        text: "Welcome to BoxFürEI"
        font.pointSize: 22
        font.italic: true
    }
    Text {
        id: textWelcome2
        x: 169
        anchors.top: textWelcome.bottom
        anchors.topMargin: 9
        text: "Please scan your StudentCard"
        font.pointSize: 14
    }

    // Check if the user is allowed to open the closet
    TextField {
        id: childTextField
        x: 141
        anchors.top: textWelcome2.bottom
        anchors.topMargin: 59
        width: 358
        height: 20
        text: ""
        echoMode: 2
        onAccepted: {
            // If the user type "test", restart the "timer1" of the main page, start the function "launchGPIO" (which is defined in "script.sh")
            // And pop this page out of the stack
            if (childTextField.text === "test") {
                timer1.restart()
                // "launchGPIO" only is used in the Raspberry device!
//                testFunctions.launchGPIO()
                stackView.pop()
            }
            else {
                messageDialog.open()
                messageDialogTimer.start()      // Start the timer for the warning message dialog, so it closes automatically
            }
        }
        Component.onCompleted: forceActiveFocus()
    }

    // MessageDialog in case that the used isn't allowed to open the close
    MessageDialog {
        id: messageDialog
        icon: StandardIcon.Warning
        title: "Wrong StudentCard"
        text: "Woah woah, what do you think you're doing? Only EI Students and Tutors allowed ;) ."
        standardButtons: StandardButton.Discard
        onDiscard: childTextField.text = ""
    }

    // This timer is set together with the MessageDialog
    Timer {
        id: messageDialogTimer
        interval: 5 * 1000 // 5 * 1000 ms = 5 sec
        onTriggered: {
            messageDialog.close()
            childTextField.text = ""
        }
    }
}
