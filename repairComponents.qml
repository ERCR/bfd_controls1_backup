import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import AppLauncher 1.0
import QtQuick.Controls.Styles 1.2

Loader {
    id: repairComponents
    property string information: "repairComponents"         // This tag is used to identify this page on the stack
    property string boxNumber

    ScriptLauncher {
        id: testFunctions
    }

    // The MouseArea is used to control how much time has passed since the user last touched the screen
    MouseArea {
        anchors.fill: parent
        onClicked: {
            timer1.restart()
        }
    }

    // Settings of the date and time funtion
    Timer {
        id: dateAndTime
        interval: 500
        running: true
        repeat: true
        onTriggered: {
            dateTime2.text = new Date().toLocaleTimeString(Qt.locale("de_DE"), "hh:mm:ss")
            dateTime1.text = new Date().toLocaleDateString(Qt.locale("de_DE"))
        }
    }
    RowLayout {
        anchors.right: parent.right
        anchors.rightMargin: 8

        Text {
            id: dateTime1
        }
        Text {
            id: dateTime2
            font.bold: true
            style: Text.Normal
            font.pointSize: 9
        }
    }

    // Set the splitview of the page. The left side contains the table of components.
    // The right side displays the picture of the component and it's where the Text(s) and Textfield(s) are placed
    SplitView {
        id: splitView
        anchors.topMargin: 50
        anchors.fill: parent
        width: Screen.width

        Rectangle {
            Layout.minimumWidth: 380
            z: 100                          // Set the left side in top of the right side
            color: "white"
            TabView {
                id: tabView
                anchors.fill: parent
                anchors.margins: 10
                enabled: false
                Tab {
                    id: compTab
                    title: "Call for a check"
                    anchors.margins: 10
                    TableView {
                        id: tableView
                        onClicked: {
                            // When the name of the component is clicked, the image is displayed on the right side
                            imageView.scale = 1
                            imageView.y = 25
                            imageView.x = 0
                            imageView.source =  listModel.get(tableView.currentRow).source

                        }

                        // The "model" is where the information of the table is saved
                        model: ListModel {
                            id: listModel
                            ListElement {
                                nameComp: "Atmel port"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/atmel.png"
                            }
                            ListElement {
                                nameComp: "Bending tool"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/bend.png"
                            }
                            ListElement {
                                nameComp: "Capacitor 1uF"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/cond10n.png"
                            }
                            ListElement {
                                nameComp: "Capacitor 10nF"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/cond10n.png"
                            }
                            ListElement {
                                nameComp: "Capacitor 10uF"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/cond10u.png"
                            }
                            ListElement {
                                nameComp: "Capacitor 100nF"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/cond100n.png"
                            }
                            ListElement {
                                nameComp: "Capacitor 100uF"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/cond100u.png"
                            }
                            ListElement {
                                nameComp: "Digital number display"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/dignum.png"
                            }
                            ListElement {
                                nameComp: "Diode"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/diode.png"
                            }
                            ListElement {
                                nameComp: "Electrical fuse"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/fuse.png"
                            }
                            ListElement {
                                nameComp: "Infrared sensor"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/sensor.png"
                            }
                            ListElement {
                                nameComp: "LCD display"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/lcd.png"
                            }
                            ListElement {
                                nameComp: "Green LED"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/ledg.png"
                            }
                            ListElement {
                                nameComp: "Red LED"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/ledr.png"
                            }
                            ListElement {
                                nameComp: "White LED"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/ledw.png"
                            }
                            ListElement {
                                nameComp: "Microcontroller"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/octo.png"
                            }
                            ListElement {
                                nameComp: "Motor"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/motor.png"
                            }
                            ListElement {
                                nameComp: "Multimeter"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/multi.png"
                            }
                            ListElement {
                                nameComp: "NPN Transistor (547c)"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/npn547.png"
                            }
                            ListElement {
                                nameComp: "NPN Transistor (557c)"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/npn557.png"
                            }
                            ListElement {
                                nameComp: "Oscillator 16MHz"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/quartz.png"
                            }
                            ListElement {
                                nameComp: "Pincer"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/pincer.png"
                            }
                            ListElement {
                                nameComp: "Push button (big)"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/pushb.png"
                            }
                            ListElement {
                                nameComp: "Push button (small)"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/pushbsm.png"
                            }
                            ListElement {
                                nameComp: "Resistor 1kOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res1k.png"
                            }
                            ListElement {
                                nameComp: "Resistor 1MOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res1M.png"
                            }
                            ListElement {
                                nameComp: "Resistor 5kOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res5k.png"
                            }
                            ListElement {
                                nameComp: "Resistor 10kOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res10k.png"
                            }
                            ListElement {
                                nameComp: "Resistor 33Ohm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res33.png"
                            }
                            ListElement {
                                nameComp: "Resistor 47kOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res47k.png"
                            }
                            ListElement {
                                nameComp: "Resistor 100Ohm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res100.png"
                            }
                            ListElement {
                                nameComp: "Resistor 100kOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res100k.png"
                            }
                            ListElement {
                                nameComp: "Resistor 200Ohm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res200.png"
                            }
                            ListElement {
                                nameComp: "Resistor 330Ohm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res330.png"
                            }
                            ListElement {
                                nameComp: "Resistor 470kOhm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res470k.png"
                            }
                            ListElement {
                                nameComp: "Resistor 620Ohm"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/res620.png"
                            }
                            ListElement {
                                nameComp: "Screwdriver"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/screw.png"
                            }
                            ListElement {
                                nameComp: "Tweezer"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/tweezer.png"
                            }
                            ListElement {
                                nameComp: "USB-Mini USB cable"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/usb.png"
                            }
                            ListElement {
                                nameComp: "USB-USB cable"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/usbsm.png"
                            }
                            ListElement {
                                nameComp: "Various wires"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/wires.png"
                            }
                            ListElement {
                                nameComp: "Zener diode"
                                source: "file://home/pi/Documents/BFD/bfd_controls1_backup/componentImages/zdiode.png"
                            }
                        }

                        // Set the different columns in the table
                        TableViewColumn {
                            title: "Name"
                            width: tableView.width/3 +5
                            role: "nameComp"
                            horizontalAlignment: Text.AlignLeft
                        }
                        TableViewColumn {
                            // This column contains the checkboxes
                            title: "Repair/Missing"
                            width: tableView.width/3 -20
                            role: "repairComp"
                            horizontalAlignment: Text.AlignHCenter
                            // The "delegate" defines how the data in "model" should be displayed
                            delegate: Rectangle {
                                anchors.fill: parent
                                CheckBox {
                                    id: compCheckBox
                                    anchors.centerIn: parent
                                    onClicked: {
                                        // When clicked, the data is sent to the console
                                        console.log("The current index is: " + styleData.row)
                                    }
                                }
                            }
                        }
                        TableViewColumn {
                            // This columns contains the text fields for the description
                            title: "Description"
                            width: tableView.width/3 +15
                            role: "description"
                            horizontalAlignment: Text.AlignHCenter
                            delegate: TextField {
                                id: compTextField
                                placeholderText: "Press enter at the end"
                                font.pointSize: 7
                                onAccepted: {
                                    console.log("This was the text: " + compTextField.text)
//                                    testFunctions.launchKeyboard("matchbox-keyboard")
                                }
                            }
                        }
                    }
                }

                // This tab serves no purpose. It can be later used for other functions
                Tab {
                    title: "Status"
                }
            }
        }

        // The flickable property allows the hand gestures on the touchscreen
        Flickable {
            id: imageFrame
            Layout.minimumWidth: 200

            Image {
                id: imageView
                fillMode: Image.PreserveAspectFit
                width: parent.width
                y: 25
            }
            PinchArea {
                anchors.fill: imageView
                pinch.target: imageView
                pinch.minimumScale: 0.1
                pinch.maximumScale: 10
                pinch.dragAxis: Pinch.XAndYAxis
                MouseArea {
                    // When the image is clicked, it will zoom in
                    anchors.fill: parent
                    drag.target: imageView
                    onClicked: {
                        imageView.scale += 0.2
                    }
                }
            }

            // Text and Textfield(s) for the Studentcard, Boxnumber and "OK/Cancel" buttons
            RowLayout {
                id: studentTextAndTextField
                height: 28
                spacing: 5
                anchors.bottom: boxTextAndTextfield.top
                anchors.bottomMargin: 6
                anchors.right: parent.right
                anchors.rightMargin: 9

                Text {
                    text: "Tutor StudentCard: "
                    font.pointSize: 8
                }
                TextField {
                    id: studentTextline
                    width: 350
                    enabled: true
                    placeholderText: "Scan StudentCard "
                    font.pointSize: 8
                    echoMode: 2
                    onAccepted: {
                        // Check the information and enable the next textfield
                        // Check rights of studentCard!!
                        boxNumberTextline.enabled = true
                        boxNumberTextline.forceActiveFocus()
                        console.log(studentTextline.text)
                    }
                }
            }

            RowLayout {
                id: boxTextAndTextfield
                height: 28
                spacing: 5
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 120
                anchors.right: parent.right
                anchors.rightMargin: 9

                Text {
                    text: "Box Number: "
                    font.pointSize: 8
                }
                TextField {
                    id: boxNumberTextline
                    width: 350
                    enabled: false
                    placeholderText: "Scan Box Number "
                    font.pointSize: 8
                    onAccepted: {
                        // Save the boxNumber info and enable the table
                        boxNumber = boxNumberTextline.text
                        console.log(boxNumber)
                        tabView.enabled = true
                    }
                }
            }

            // Set the "OK/Cancel" buttons
            // Currently useless
            RowLayout {
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 65
                anchors.right: parent.right
                anchors.rightMargin: -12
                width: parent.width
                spacing: 5
                Button {
                    id: acceptButton
                    width: 80
                    height: 45

                    text: "Accept"
                    style: ButtonStyle {
                        background : Rectangle {
                            radius: 4
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                            }
                        }
                    }
                    onClicked: {

                    }
                }
                Button {
                    id: cancelButton
                    width: 80
                    height: 45

                    text: "Cancel"
                    style: ButtonStyle {
                        background : Rectangle {
                            radius: 4
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                            }
                        }
                    }
                    onClicked: {

                    }
                }
            }
        }
    }
}
