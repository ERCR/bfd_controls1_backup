#ifndef SCRIPTLAUNCHER_H
#define SCRIPTLAUNCHER_H

#include <QObject>
#include <QProcess>

// Define the class "Scriptlauncher" in C++ and its functions. They will be later passed on qml.

class ScriptLauncher : public QObject
{
    Q_OBJECT
public:
    explicit ScriptLauncher(QObject *parent = 0);
    Q_INVOKABLE void launchGPIO();
    Q_INVOKABLE void launchKeyboard(const QString &application);

signals:

public slots:

private:
    QProcess *m_process;
    QProcess *m_keyboard;
};

#endif // SCRIPTLAUNCHER_H
